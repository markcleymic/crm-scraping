# coding: utf8

from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.views.generic import ListView
from .models import Information, Entreprise_model
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

import os
import unicodedata
import csv
import time
import requests



class web_crawler(ListView):
    model = Entreprise_model 
    template_name = 'polls/scrape.html'

    userid = None
    password = None

    def post(self, request):

        def creamail(Nom, domaine, separation):
            Mail = Nom.replace('.','')
            Mail = Nom.replace(' ',separation)+domaine
            Mail = unicodedata.normalize('NFD', Mail)
            return(str(Mail))

        def mailtester(Mail):
            try:

                headers = {
                    'Connection': 'keep-alive',
                    'Cache-Control': 'max-age=0',
                    'Origin': 'http://mailtester.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                    'Referer': 'http://mailtester.com/testmail.php',
                    'Accept-Language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
                }

                data = {
                'lang': 'fr',
                'email': Mail
                }

                response = requests.post('http://mailtester.com/testmail.php', headers=headers, data=data)
                soup = BeautifulSoup(response.text, 'html.parser')
                bool = False
                k = 0
                test = soup.findAll('td', attrs={"class":None})[-1].text
            except Exception as e:
                test = str(e);
            finally:
                return(test)

        def lien(recherche, typecompte):
            recherche = recherche.replace(' ', '%20')
            if typecompte == "Premium":
                lien_recherche = "https://www.linkedin.com/sales/search/people?doFetchHeroCard=false&keywords="+recherche+"&page="
            if typecompte == "Normal":
                lien_recherche = "https://www.linkedin.com/search/results/people/?keywords="+recherche+"&origin=GLOBAL_SEARCH_HEADER&page="
            return(lien_recherche)

        def inversion(Nom):
            Nom = Nom[Nom.find(" ")+1:len(Nom)]+" "+Nom[0:Nom.find(" ")]
            return(Nom)

        def premierlettre(Nom):
            test = Nom[0:1]+" "+Nom[Nom.find(" ")+1:len(Nom)]
            return(test)
            
        def autremail(testNom, domaine, separation):
            Mail = creamail(testNom, domaine, separation)
            Validite = mailtester(Mail)
            return[Mail, Validite]

        def testfinal(Nom, Position, Lieu, separation, domaine, Entreprise):
            found = False
            csv_file = open(nom_csv, 'r')
            for line in csv_file:
                if Nom in line and Position in line:
                    found = True
                    break

            if found == False:
                if "France" in Lieu:
                    print(Nom)
                    #verification mail avec different type de format ( prenom.nom, nom.prenom, P.nom )
                    Mail = creamail(Nom, domaine, separation)
                    Validite = mailtester(Mail)
                    if Validite != "E-mail address is valid":
                        nb = Nom.count(' ')
                        if nb == 1 :
                            testNom = inversion(Nom)
                            divide = autremail(testNom, domaine, separation)
                            Mail = divide[0]
                            Validite = divide[1]
                            if Validite != "E-mail address is valid":
                                testNom = premierlettre(Nom)
                                divide = autremail(testNom, domaine, separation)
                                Mail = divide[0]
                                Validite = divide[1]
                    
                    print(Mail, Validite)  

                # ajout des informations dans un csv si non présent dans le csv
                
                    with open(nom_csv, 'a') as csvFile:
                        dict_writer = csv.writer(csvFile, dialect='myDialect')
                        dict_writer.writerow([Mail, Nom, Position, Lieu, Entreprise ,Validite])
                    csvFile.close()
                    #ajout des informations dans la base de donnée
                    New = Information.objects.create(
                            Mail = Mail,
                            Noms = Nom,
                            Position = Position,
                            Lieu = Lieu,
                            Entreprise = Entreprise,
                            Validite = Validite
                        )
        
        # Premium
        def Premiumscrape(Entreprisebis, domainebis, nom_csv, recherchebis, driverbis):
            separation = request.POST.get('separation')
            for i in range(int(request.POST.get('min')),int(request.POST.get('max'))+1):
                
                # redirection
                print("Redirection")
                driverbis.get(recherchebis+str(i))

                #chargement de la page
                time.sleep(2) # Pause
                driverbis.execute_script("window.scrollTo(0, 1080)")
                time.sleep(2) # Pause
                driverbis.execute_script("window.scrollTo(1080, 2160)")
                time.sleep(2) # Pause
                driverbis.execute_script("window.scrollTo(2160,3240)")
                time.sleep(2) # Pause
                driverbis.execute_script("window.scrollTo(3240,4320)")
                profils = driverbis.find_elements_by_xpath("//li[@class='pv5 ph2 search-results__result-item']")

                #récupération d'information
                for profil in profils:
                    Nom = str(profil.find_element_by_css_selector("dt.result-lockup__name").text).title()
                    Nom = Nom.replace('. ', ' ')
                    Position = str(profil.find_element_by_css_selector("dd.result-lockup__highlight-keyword").text)
                    Position = Position[0:Position.find("Accéder")]
                    Lieu = str(profil.find_element_by_css_selector("li.result-lockup__misc-item").text)
                    
                    testfinal(Nom, Position, Lieu, separation, domainebis, Entreprisebis)

        # Normal
        def Normalscrape(Entreprisebis, domainebis, nom_csv, recherchebis, driverbis):
            separation = request.POST.get('separation')
            for i in range(int(request.POST.get('min')),int(request.POST.get('max'))+1):
                    
                # redirection
                print("Redirection")
                driverbis.get(recherchebis+str(i))

                #chargement de la page
                time.sleep(2) # Pause
                driverbis.execute_script("window.scrollTo(0, 1080)")
                time.sleep(2) # Pause

                driverbis.execute_script("window.scrollTo(0, 1080)") # Pour être sur
                profils = driverbis.find_elements_by_css_selector("div.search-result__wrapper")

                #scrap information
                for profil in profils:
                    Nom = str(profil.find_element_by_css_selector("span.actor-name").text).title()
                    Nom = Nom.replace('. ', ' ')
                    Position = str(profil.find_element_by_css_selector("p.subline-level-1").text)
                    Lieu = str(profil.find_element_by_css_selector("p.subline-level-2").text)
                    
                    testfinal(Nom, Position, Lieu, separation, domainebis, Entreprisebis)

        userid = request.POST.get('mail')
        password = request.POST.get('pwd')
        nom_csv = "Contact_Linkedin.csv"

        csv.register_dialect('myDialect',
        delimiter = ';',
        skipinitialspace=True)

        with open(nom_csv, 'w') as csvFile:
            test_writer = csv.writer(csvFile, 'myDialect')
            test_writer.writerow(['Mail','Noms','Position', 'Lieu','Entreprise','Validité'])

        if userid != self.userid and password != self.password :
            # connection
            chrome_path = './chromedriver.exe'
            driver = webdriver.Chrome(chrome_path)
            driver.get("https://www.linkedin.com/login")
            driver.find_element_by_xpath("""//*[@id="username"]""").send_keys(userid)
            driver.implicitly_wait(6)
            driver.find_element_by_xpath("""//*[@id="password"]""").send_keys(password)
            driver.find_element_by_xpath("""//*[@class="btn__primary--large from__button--floating"]""").click()

        Entreprise = request.POST.get('nomentreprise')
        domaine = request.POST.get('domaine')

        # test de l'existance du nom de l'entreprise et du nom de domaine dans la base de donnée
        test_ex, test_nom = True, True
        try:
            listing = Entreprise_model.objects.get(Nom_entreprise = Entreprise)
        except Entreprise_model.DoesNotExist:
            test_nom = False
        try:
            listing = Entreprise_model.objects.get(extension = domaine)
        except Entreprise_model.DoesNotExist:
            test_ex = False
        
        if test_nom == False and test_ex == False:
            New = Entreprise_model.objects.create(
                        Nom_entreprise = Entreprise,
                        extension = domaine
                    )
        if test_nom == False and test_ex == True:
            New = Entreprise_model.objects.create(
                        Nom_entreprise = Entreprise
                    )
        if test_nom == True and test_ex == False:
            New = Entreprise_model.objects.create(
                        extension = domaine
                    )

        recherche = lien(request.POST.get('research'),request.POST.get('type'))

        if request.POST.get('type') == "Premium":
            Premiumscrape(Entreprise, domaine, nom_csv ,recherche, driver)
        if request.POST.get('type') == "Normal":
            Normalscrape(Entreprise, domaine, nom_csv ,recherche, driver)
        
        return HttpResponseRedirect('/scrape')


  