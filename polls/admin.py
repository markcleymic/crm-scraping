from django.contrib import admin
from .models import Information, Entreprise_model


class PageAdminInformation(admin.ModelAdmin):
    list_display = ('Mail', 'Entreprise', 'Validite')
    list_display_links = ('Mail','Entreprise')
    list_per_page = 25

class PageAdminEntreprise(admin.ModelAdmin):
    list_display = ('Nom_entreprise', 'extension')
    list_display_links = ('Nom_entreprise', 'extension')
    list_per_page = 25
    
admin.site.register(Information, PageAdminInformation)
admin.site.register(Entreprise_model, PageAdminEntreprise)
