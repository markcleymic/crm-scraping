from django.db import models


class Information(models.Model):
    Mail = models.CharField(max_length=50)
    Noms = models.CharField(max_length=50)
    Position = models.CharField(max_length=100)
    Lieu = models.CharField(max_length=50)
    Entreprise = models.CharField(max_length=50)
    Validite = models.CharField(max_length=100)
    def __str__(self):
        return self.Mail

class Entreprise_model(models.Model):
    Nom_entreprise = models.CharField(max_length=50, blank=True)
    extension = models.CharField(max_length=50, blank=True)
    def __str__(self):
        return self.Nom_entreprise