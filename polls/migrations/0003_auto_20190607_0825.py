# Generated by Django 2.2 on 2019-06-07 08:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_entreprise_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entreprise_model',
            name='Nom_entreprise',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='entreprise_model',
            name='extension',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
