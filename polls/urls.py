from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.web_crawler.as_view(), name='home'),
]